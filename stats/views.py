from __future__ import unicode_literals
from django.shortcuts import render
# -*- coding: utf-8 -*-
from django.utils.encoding import *
import re
from django.shortcuts import render, get_object_or_404
from .models import Words
from django.db.models import Count
# Create your views here.
from bs4 import BeautifulSoup
from urllib.request import urlopen
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
import json
from .tasks import infof

infof()


def stats(request):
    words = Words.objects.values("word").annotate(num_words=Count('word')).order_by('-num_words')[:10]
    word_dictionary = {}
    word_dictionary2 = {}
    i = 0
    for item in words:
        a = list(item.values())[0]
        b = list(item.values())[1]
        word_dictionary[a] = b

    # Create a list of tuples sorted by index 1 i.e. value field  --> from dictionary
    list_of_Tuples = sorted(word_dictionary.items(), reverse=True, key=lambda x: x[1])[
                     :10]  # reverse  is true it means that the order will be decreasing
    for elem in list_of_Tuples:
        word_dictionary2[elem[0]] = elem[1]

    data = word_dictionary2
    return JsonResponse(data, json_dumps_params={'indent': 2})


def authors(request):
    word_dictionary = {}
    authors = Words.objects.values("author").order_by('author').distinct('author')
    i = 0

    for item in authors:
        a = list(item.values())[0]
        mySeparator = ""
        a = (mySeparator.join(a.split())).lower()
        b = list(item.values())[0]
        word_dictionary[a] = b
        i = i + 1

    data = word_dictionary
    return JsonResponse(data, json_dumps_params={'indent': 2})


def author(request, author):
    object_list = Words.objects.all()
    dict_author = author
    word_dictionary = {}
    word_dictionary2 = {}

    for item in object_list:
        c = item.author
        mySeparator = ""
        d = (mySeparator.join(c.split())).lower()
        if dict_author == d:
            if item.word in word_dictionary:
                word_dictionary[item.word] += 1
            else:
                word_dictionary[item.word] = 1

    # Create a list of tuples sorted by index 1 i.e. value field  --> from dictionary
    list_of_Tuples = sorted(word_dictionary.items(), reverse=True, key=lambda x: x[1])[
                     :10]  # reverse  is true itmeans that the order will be decreasing
    for elem in list_of_Tuples:
        word_dictionary2[elem[0]] = elem[1]

    data = word_dictionary2
    return JsonResponse(data, json_dumps_params={'indent': 2})






