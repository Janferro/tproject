from django.shortcuts import render
from django.urls import path
from . import views
#from rest_framework.urlpatterns import format_suffix_patterns


app_name = 'teproject'
urlpatterns = [
    path("stats/", views.stats, name="stats"),
    path("authors/", views.authors, name="authors"),
    path("stats/<str:author>/", views.author, name="author"),
]