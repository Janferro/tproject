from __future__ import absolute_import, unicode_literals
import re
from .models import Words
from bs4 import BeautifulSoup
from urllib.request import urlopen
from celery import shared_task


@shared_task
def infof():

    object_list = Words.objects.all()

    def parse():

        words_auth_list = []
        html = urlopen('https://teonite.com/blog/')
        bs = BeautifulSoup(html, 'html.parser')
        namelist = bs.find('span', {'class': 'page-number'}).get_text()
        i = 0
        j = 1
        k = 1
        num = 0
        #number of pages
        if "/" in namelist:
            ns = namelist.split("/")
            del ns[0]
            num_pages = int(ns[0])
        else:
            for i in namelist:
                if i.isnumeric():
                    num = num + i
            num_pages = num
                
        while j <= 3: #only three:  num_pages
            j += 1
            for link in bs.find_all('h2', {'class': 'post-title'}):
             #https: // teonite.com / blog /

                l = link.find('a', href=re.compile('^(/blog/)*'))
                if 'href' in l.attrs:
                    html = urlopen('https://teonite.com{}'.format(l.attrs['href'][8:]))
                    bs1 = BeautifulSoup(html, 'html.parser')
                    name = bs1.find('span', {"author-name"}).get_text()

                    text = bs1.find('div', {'class': 'post-content'}).get_text()
                    text_list = text.split()

                    for i in range(len(text_list)):
                        if len(text_list[i]) > 3 and text_list[i].isalpha():
                            words_auth_list.append(name)
                            words_auth_list.append(text_list[i])
            print('https://teonite.com/blog/' + 'page/' + str(j) + '/index.html', j)
            html = urlopen('https://teonite.com/blog/' + 'page/' + str(j) + '/index.html')
            bs = BeautifulSoup(html, 'html.parser')



        i = 0
        while len(words_auth_list):
            author = words_auth_list[0]
            name = words_auth_list[1]
            words = Words(word=name, author=author)  # uzupełnianie bazy danych
            words.save()
            del words_auth_list[:2]


    if object_list:

        Words.objects.all().delete()
        parse()

    else:
        parse()

    print("koniec")

