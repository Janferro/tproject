from __future__ import unicode_literals
from django.db import models

# Create your models here.
# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
from django.urls import reverse


class Words(models.Model):
    word = models.CharField(max_length=20)
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250,
                            unique_for_date='publish')
    author = models.CharField(max_length=50)
    publish = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ('-publish',)

    def __str__(self):
        return self.title
