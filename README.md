**1. Cel programu:**

Zebranie danych z każdego artykułu z bloga https://teonite.com/blog/, przetworzenie ich i zaprezentowanie w postaci statystyk (wyliczanych z bazy danych).
Dane są zbierane w bazie danych (PostgreSQL) przez przetwarzanie poszczególnych dokumentów HTML zwracanych przez https://teonite.com/blog/, (tzw. Web scraping).

**2. Finalnie serwis typu REST zwraca następujące statystyki w postaci dokumentu JSON:**

*  10 najczęściej występujących słów z ich liczebnością dostępne pod adresem /stats/

*  10 najczęściej występujących słów z ich liczebnością per autor dostępne pod adresem /stats/<author>/

*  autorzy postów wraz z ich nazwą dostępną w adresie /stats/<author>/ dostępna pod adresem /authors/

  
**3. Program był zrobiony w PyCharmie, pod Linuxem (Ubuntu 18.4).**

**4. Po sklonowaniu programu i otwarciu w terminalu katalogu "teproj" utworzyłem srodowisko wirtualne i wgrywałem aplikacje w kolejności przedstawionej ponizej:** 

01. ~/teproj$ python3 -m venv env
02. ~/teproj$ source env/bin/activate
03. (env) ~/teproj$ pip3 install -r requirements.txt

**5. Uruchomienie**

(env) ~/teproj$ python3 manage.py runserver

UWAGI:

    Przed uruchomieniem trzeba utworzyc baze danych - najlepiej tylko dla modelu zawartego w pliku models.py (reszte wgrac pozniej)

Przykład odpowiedzi:
np. dla *http://127.0.0.1:8080/stats/*

{
"jest": 3455,
"super": 4566,
"tra": 2323,
"la": 4545,
"lala": 4545,
"heja": 8979,
"ho": 9090,
"pa": 2323,
"papa": 4545
}

dla *http://127.0.0.1:8080/authors/*

{
"janszeba": "Jan Szeba",
"andrzejnowak": "Andrzej Nowak",
"karolinakozka": "Karolina Kozka"
}

a dla *http://127.0.0.1:8080/stats/janszeba*

{
"jest": 444,
"super": 333,
"tra": 234,
"la": 456,
"lala": 678,
"heja": 123,
"ho": 4564,
"pa": 55,
"papa": 345
}

Nie zaimplementowano jeszcze kodu obsługującego Celery.


